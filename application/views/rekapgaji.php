<?php 
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=laporan_gaji.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");

?>


<table class="table" id="example">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>Gaji</th>
            <th>Jumlah Absen</th>
            <th>Jumlah Gaji</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i=1;
        foreach ($rekap as $value) { ?>
        <tr>
            <th scope="row"><?php echo $i; ?></th>
            <td><?php echo $value['peg_nama']; ?></td>
            <td><?php echo $value['peg_jabatan']; ?></td>
            <td>
                <?php
								
                foreach ($gaji as $Hgaji) {
                    if ($value['peg_id'] == $Hgaji['peg_id']) {
                    $gajigan = $Hgaji['gaji_pegawai'];
                    echo "Rp " . number_format($gajigan,2,',','.');
                    break;
                    }  
                }
                ?>
            </td>
            <td><?php echo $value['hasil']; ?></td>
            <td><?php 
            if($value['progres_absen'] == 1){
                $jumlah = $gajigan * $value['hasil'];
                echo "Rp " . number_format($jumlah,2,',','.');		
            }
            
            ?></td>
        </tr>
        <?php
        $i++;
        }
        ?>
    </tbody>
</table>