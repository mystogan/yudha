<!DOCTYPE HTML>
<html>
<head>
<title>Glance Design Dashboard an Admin Panel Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
<link href='<?php echo base_url(); ?>assets/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
</head>

<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
		<!--left-fixed -navigation-->
		<aside class="sidebar-left">
      <nav class="navbar navbar-inverse">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <h1><a class="navbar-brand" href="<?php echo base_url(); ?>"><span class=""></span>SIMKONTRUKSI<span class="dashboard_text"></span></a></h1>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="sidebar-menu">
              <li class="header">MAIN NAVIGATION</li>
              <li class="treeview">
                <a href="<?php echo base_url(); ?>">
                <i class=""></i> <span>Dashboard</span>
                </a>
              </li>
              <li class="treeview">
                <a href="<?php echo base_url(); ?>home/pegawai">
                <i class=""></i> <span>Master Pegawai</span>
                </a>
              </li>
              <li class="treeview">
                <a href="<?php echo base_url(); ?>home/material">
                <i class=""></i> <span>Master Material</span>
                </a>
              </li>
              <li class="treeview">
                <a href="<?php echo base_url(); ?>home/proyek">
                <i class=""></i> <span>Proyek</span>
                </a>
              </li>
              <?php
          		$level = $this->session->userdata('level');
              if ($level == 1 || $level == 2 || $level == 3 ) { ?>
              <li class="treeview">
                <a href="<?php echo base_url(); ?>home/rekap">
                <i class=""></i> <span>Rekap Gaji</span>
                </a>
              </li>
              <li class="treeview">
                <a href="<?php echo base_url(); ?>home/rekapAbsen">
                <i class=""></i> <span>Rekap Absen</span>
                </a>
              </li>
              
              <?php
              }
              ?>
              
            </ul>
          </div>
          <!-- /.navbar-collapse -->
      </nav>
    </aside>
	</div>
		<!--left-fixed -navigation-->

    <!-- header-starts -->
  <div class="sticky-header header-section ">
    <div class="header-left">

      <div class="profile_details_left"><!--notifications of menu start -->

        <div class="clearfix"> </div>
      </div>
      <!--notification menu end -->
      <div class="clearfix"> </div>
    </div>
    <div class="header-right">


      <div class="profile_details">
        <ul>
          <li class="dropdown profile_details_drop">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <div class="profile_img" style="margin-right: -50px;">
                <span class="prfil-img"><img src="<?php echo base_url();?>assets/images/2.jpg" alt=""> </span>
                <div class="user-name">
                  <p><?php echo $this->session->userdata('username');?></p>
                  <span>Application</span>
                </div>
                <div class="clearfix"></div>
              </div>
            </a>
            <ul class="dropdown-menu drp-mnu">
              <li> <a href="<?php echo base_url();?>home/gantiPassword">Ganti Password</a> </li>
              <li> <a href="<?php echo base_url();?>home/logout"> Logout</a> </li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>
  </div>
  <!-- //header-ends -->
