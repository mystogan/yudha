

<!-- main content start-->
<div id="page-wrapper">
			<div class="main-page">
				<div class="tables">
					<h2 class="title1">Tables</h2>

					<div class="table-responsive bs-example widget-shadow col-md-6">
            <a href="javascript:tambah_pegawai();">
              <button class="btn btn-success" type="button" name="button" data-toggle="modal" data-target="#myModal" >Tambah</button>
            </a>
            <hr>
    				<h4>Data Pegawai (<?php echo $proyek['proyek_nama']; ?>):</h4>
					<table class="table">
							<thead>
								<tr>
								  <th>No</th>
								  <th>Nama Pegawai</th>
								  <th>Detail Pegawai</th>
								  <th>Lama Pegawai</th>
								  <th>Gaji Pegawai</th>
								  <th></th>
								</tr>
							</thead>
							<tbody>
                <?php
                $i=1;
                foreach ($pegawai as $value) { ?>
                  <tr>
                    <th scope="row"><?php echo $i; ?></th>
                    <td><?php echo $value['peg_nama']; ?></td>
                    <td><?php echo $value['detail_pegawai']; ?></td>
                    <td><?php echo $value['lama_pegawai']; ?> Minggu</td>
                    <td><?php echo $value['gaji_pegawai']; ?></td>
                    <td>
                      <a href="javascript:edit_pegawai('<?php echo $value['dp_id']; ?>','<?php echo $value['id_pegawai']; ?>','<?php echo $value['detail_pegawai']; ?>','<?php echo $value['lama_pegawai']; ?>');">
                        <button class="btn btn-warning" type="button" name="button"  data-toggle="modal" data-target="#myModal"  >Edit</button>
                      </a>
                    </td>
                  </tr>

                <?php
                $i++;
                }
                ?>
							</tbody>
						</table>
						</div>
					<div class="table-responsive bs-example widget-shadow col-md-6">
            <a href="javascript:tambah_material();">
              <button class="btn btn-success" type="button" name="button" data-toggle="modal" data-target="#material" >Tambah</button>
            </a>
            <hr>
						<h4>Data Material (<?php echo $proyek['proyek_nama']; ?>):</h4>
						<table class="table">
							<thead>
								<tr>
								  <th>No</th>
								  <th>Nama Material</th>
								  <th>Banyak</th>
								  <th>Keterangan</th>
								  <th></th>
								</tr>
							</thead>
							<tbody>
                <?php
                $i=1;
                foreach ($material as $value) { ?>
                  <tr>
                    <th scope="row"><?php echo $i; ?></th>
                    <td><?php echo $value['master_nama']; ?></td>
                    <td><?php echo $value['banyak']." ".$value['master_satuan']; ?></td>
                    <td><?php echo $value['ket']; ?></td>
                    <td>
                      <a href="javascript:edit_material('<?php echo $value['dp_id']; ?>','<?php echo $value['id_material']; ?>','<?php echo $value['banyak']; ?>','<?php echo $value['ket']; ?>');">
                        <button class="btn btn-warning" type="button" name="button"  data-toggle="modal" data-target="#material"  >Edit</button>
                      </a>
                    </td>
                  </tr>

                <?php
                $i++;
                }
                ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

<!-- modal gan  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
    <form class="form-horizontal" action="<?php echo base_url(); ?>home/tambah_detilProyekpegawai" method="post">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
    <input type="hidden" name="id_proyek" id="id_proyek" value="<?php echo $proyek['proyek_id']; ?>">
    <input type="hidden" name="id_detilpegawai" id="id_detilpegawai" >
	  <div class="modal-body">
      <div class=" form-grids row form-grids-right">
					<div class="widget-shadow " data-example-id="basic-forms">
						<div class="form-title">
							<h4>Form Pegawai:</h4>
						</div>
						<div class="form-body">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Nama Proyek</label>
                <div class="col-sm-9">
                  <select class="form-control" name="id_pegawai" id="id_pegawai">
                    <?php
                    foreach ($pegawaiall as $value) { ?>
                      <option value="<?php echo $value['peg_id']; ?>"><?php echo $value['peg_nama']; ?></option>
                    <?php
                    }
                    ?>
                  </select>
                </div>
                </div>

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Lama Pekerjaan</label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" name="lama_pegawai" id="lama_pegawai" placeholder="Masukkan Lama Pekerjaan">
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Gaji Pegawai</label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" name="gaji_pegawai" id="gaji_pegawai" placeholder="Masukkan Gaji Pekerjaan">
                </div>
              </div>

            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="detail_pegawai" id="detail_pegawai" placeholder="Masukkan Detail">
              </div>
            </div>
						</div>
					</div>
				</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save</button>
	  </div>
  </form>
	</div>
  </div>
</div>
<div class="modal fade" id="material" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
    <form class="form-horizontal" action="<?php echo base_url(); ?>home/tambah_detilProyekmaterial" method="post">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
    <input type="hidden" name="id_proyek" id="id_proyek" value="<?php echo $proyek['proyek_id']; ?>">
    <input type="hidden" name="id_detilmaterial" id="id_detilmaterial" >
	  <div class="modal-body">
      <div class=" form-grids row form-grids-right">
					<div class="widget-shadow " data-example-id="basic-forms">
						<div class="form-title">
							<h4>Form Material:</h4>
						</div>
						<div class="form-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Material</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="id_material" id="id_material">
                      <?php
                      foreach ($materialall as $value) { ?>
                        <option value="<?php echo $value['master_id']; ?>"><?php echo $value['master_nama']; ?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                  </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Banyak Material</label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control" name="banyak" id="banyak" placeholder="Masukkan Banyak Material">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="ket" id="ket" placeholder="Masukkan Keterangan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Harga</label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control" name="harga" id="harga" placeholder="Masukkan Harga Material">
                  </div>
                </div>
						</div>
					</div>
				</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save</button>
	  </div>
  </form>
	</div>
  </div>
</div>

<!-- end modal -->


<script type="text/javascript">
  function tambah_pegawai(){
    $("#id_detilpegawai").val("");
    $("#detail_pegawai").val("");
    $("#lama_pegawai").val("");
  }

  function edit_pegawai(id,id_pegawai,detail,lama){
    $("#id_detilpegawai").val(id);
    $("#id_pegawai").val(id_pegawai);
    $("#detail_pegawai").val(detail);
    $("#lama_pegawai").val(lama);
  }
  function tambah_material(){
    $("#id_detilmaterial").val("");
    $("#banyak").val("");
    $("#ket").val("");
    $("#harga").val("");
  }

  function edit_material(id,id_material,banyak,ket){
    $("#id_detilmaterial").val(id);
    $("#id_material").val(id_material);
    $("#banyak").val(banyak);
    $("#ket").val(ket);
  }
</script>
