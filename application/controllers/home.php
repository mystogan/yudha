<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		// mengset library session untuk controller ini
		$this->load->library('session');
		// deklarasi database untuk mengecek database yang akan digunakan
		$this->load->database();
		// deklarasi model yang akan jadi query
		$this->load->model('Home_model');
		$this->load->helper(array('form','url','file','download'));
		// fungsi untuk menghapus error yang tidak dideklarasikan
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

	}

	public function index(){
		// menyimpan session user ke dalam variable $user
		$user = $this->session->userdata('username');
		// untuk mengecek apakah sudah login apa belum
		if ($user == "") {
			// redirect ke login
			header("location:".base_url()."home/login");
		}
		$id_proyek = $this->input->post('proyek');
		// $thisYear = date("Y");
		$data['proyek2'] = $this->Home_model->getproyek();
		if ($id_proyek == '') {
			$id_proyek = $data['proyek2'][0]['proyek_id'];
		}
		// untuk mendapatkan nama proyek
		$data['proyek'] = $this->Home_model->getPerproyek($id_proyek);
		$data['hasil'] = $this->Home_model->hasil($id_proyek);
		$data['jml'] = $this->Home_model->jumlahminggu($id_proyek);
		$getProgress = $this->Home_model->jmlProgres($id_proyek);
		$jmlProgress = count($getProgress);
		$data['progress_proyek'] = round((($jmlProgress/$data['jml']['bulat'])*100),2);
		// echo " ";
		$data['hasil_yang_belum'] = 100-$data['progress_proyek'];
		$data['id_proyek'] = ($id_proyek);
		// print_r ($data['jml']);
		// untuk menampilkan html ke user
		$simpan = '';
		$angka = 0;
		for ($i=0; $i < $data['jml']['bulat']; $i++) { 
			$simpan = "SELECT COUNT(progres_id) AS hasil
						FROM progres a
						JOIN master_proyek b ON b.proyek_id = a.proyek_id
						WHERE a.proyek_id = $id_proyek
						AND progres_hari >= (DATE_ADD(proyek_awal, INTERVAL $angka DAY)) AND progres_hari <= (DATE_ADD(proyek_awal, INTERVAL ($angka+7) DAY)) ";
			$query = $this->db->query($simpan);
			$dat = $query->result_array();
			$data['jumlah'][$i] = $dat[0];
			$angka = $angka+7;
		}
		$this->load->view('header',$data);
		$this->load->view('dashboard',$data);
		$this->load->view('footer',$data);

	}
	public function pegawai(){
		// menyimpan session user ke dalam variable $user
		$user = $this->session->userdata('username');
		// untuk mengecek apakah sudah login apa belum
		if ($user == "") {
			// redirect ke login
			header("location:".base_url()."home/login");
		}
		// mengambil data pegawai dari database dengan cara memanggil ke model
		$data['pegawai'] = $this->Home_model->getpegawai();

		// untuk menampilkan html ke user
		$this->load->view('header',$data);
		$this->load->view('pegawai',$data);
		$this->load->view('footer',$data);

	}
	public function material(){
		// menyimpan session user ke dalam variable $user
		$user = $this->session->userdata('username');
		// untuk mengecek apakah sudah login apa belum
		if ($user == "") {
			// redirect ke login
			header("location:".base_url()."home/login");
		}
		// mengambil data material dari database dengan cara memanggil ke model
		$data['material'] = $this->Home_model->getmaterial();
		// untuk menampilkan html ke user
		$this->load->view('header',$data);
		$this->load->view('material',$data);
		$this->load->view('footer',$data);

	}
	public function rekap(){
		// menyimpan session user ke dalam variable $user
		$user = $this->session->userdata('username');
		// untuk mengecek apakah sudah login apa belum
		if ($user == "") {
			// redirect ke login
			header("location:".base_url()."home/login");
		}
		$id_proyek = $this->input->post('proyek');
		$data['proyek2'] = $this->Home_model->getproyek();
		$data['proyek_id'] = $id_proyek;
		if ($id_proyek == '') {
			$id_proyek = $data['proyek2'][0]['proyek_id'];
		}

		// mengambil data material dari database dengan cara memanggil ke model
		$data['rekap'] = $this->Home_model->getRekap($id_proyek);
		$data['gaji'] = $this->Home_model->getPegawaiGaji($id_proyek);
		// print_r ($data['material']);
		// untuk menampilkan html ke user
		$this->load->view('header',$data);
		$this->load->view('rekap',$data);
		$this->load->view('footer',$data);

	}
	public function rekapgaji($id_proyek){
		$data['rekap'] = $this->Home_model->getRekap($id_proyek);	
		$data['gaji'] = $this->Home_model->getPegawaiGaji($id_proyek);
		$this->load->view('rekapgaji',$data);
	}
	public function proyek(){
		// menyimpan session user ke dalam variable $user
		$user = $this->session->userdata('username');
		// untuk mengecek apakah sudah login apa belum
		if ($user == "") {
			// redirect ke login
			header("location:".base_url()."home/login");
		}
		$year = ($this->input->post('thisYear'));
		if ($year == null) {
			$year = date("Y");
		}

		// mengambil data proyek dari database dengan cara memanggil ke model
		$data['proyek'] = $this->Home_model->getproyek($year);
		// untuk cek level
		$level = $this->session->userdata('level');
		if ($level == 4) {
			$view = 'proyek';
		}else if ($level == 3) {
			$view = 'proyekadmin';
		}else {
			$view = 'proyek';
		}
		$data['thisYear'] = ($year);
		
		// untuk menampilkan html ke user
		$this->load->view('header',$data);
		$this->load->view($view,$data);
		$this->load->view('footer',$data);

	}
	public function gantiPassword(){
		// menyimpan session user ke dalam variable $user
		$user = $this->session->userdata('username');
		// untuk mengecek apakah sudah login apa belum
		if ($user == "") {
			// redirect ke login
			header("location:".base_url()."home/login");
		}
		// mengambil data proyek dari database dengan cara memanggil ke model
		$data['proyek'] = $this->Home_model->getproyek();
		// untuk menampilkan html ke user
		$this->load->view('header',$data);
		$this->load->view('gantiPassword',$data);
		$this->load->view('footer',$data);

	}
	public function cekGantiPassword(){

		$password_lama = md5($this->input->post('password_lama'));
		$password_baru = md5($this->input->post('password_baru'));
		$konfirmasi_password_baru = md5($this->input->post('konfirmasi_password_baru'));
		if ($password_baru != $konfirmasi_password_baru) {
			echo "<script>alert ('Konfirmasi Password Tidak Sama !');window.location.href = '".base_url()."';</script>";			
		}else {
			$data = $this->Home_model->GantiPassword($password_lama);
			if($data == ""){
				// memberitahu user bahwa tidak aada data yang sudah dimasukkan
				echo "<script>alert ('Password Lama Tidak Cocok !');window.location.href = '".base_url()."';</script>";
			}else if($data['level'] != ""){
				$id_user = $this->session->userdata('id');
				$datas['password'] = $password_baru;
				$this->db->where('id', $id_user);
				$this->db->update('users', $datas);
				echo "<script>alert ('Password Berhasil diubah !');window.location.href = '".base_url()."';</script>";

			}else {
				
				echo "<script>window.location.href = '".base_url()."';</script>";
			}
		}

	}
	public function proses($proyek_id){
		// menyimpan session user ke dalam variable $user
		$user = $this->session->userdata('username');
		// untuk mengecek apakah sudah login apa belum
		if ($user == "") {
			// redirect ke login
			header("location:".base_url()."home/login");
		}
		// mengambil data proses dari database dengan cara memanggil ke model
		$data['proses'] = $this->Home_model->getProses($proyek_id);
		$data['proyek2'] = $this->Home_model->getproyek();
		$data['pegawai'] = $this->Home_model->getdetilproyek_pegawai($proyek_id);
		// untuk mendapatkan nama proyek
		$data['proyek'] = $this->Home_model->getPerproyek($proyek_id);
		// print_r ($data['proses']);
		// untuk menampilkan html ke user
		$this->load->view('header',$data);
		$this->load->view('proses',$data);
		$this->load->view('footer',$data);

	}
	public function rekapAbsen($proyek_id){
		// menyimpan session user ke dalam variable $user
		$user = $this->session->userdata('username');
		// untuk mengecek apakah sudah login apa belum
		if ($user == "") {
			// redirect ke login
			header("location:".base_url()."home/login");
		}
		$id_proyek = $this->input->post('proyek');
		$data['proyek2'] = $this->Home_model->getproyek();
		if ($id_proyek == '') {
			$id_proyek = $data['proyek2'][0]['proyek_id'];
		}
		$data['proyek_id'] = $id_proyek;
		// mengambil data proses dari database dengan cara memanggil ke model
		$data['proses'] = $this->Home_model->getProses($id_proyek);
		$data['proyek2'] = $this->Home_model->getproyek();
		$data['pegawai'] = $this->Home_model->getdetilproyek_pegawai($id_proyek);
		// untuk mendapatkan nama proyek
		$data['proyek'] = $this->Home_model->getPerproyek($id_proyek);
		// print_r ($data['proses']);
		// untuk menampilkan html ke user
		$this->load->view('header',$data);
		$this->load->view('rekapAbsen',$data);
		$this->load->view('footer',$data);

	}
	public function rekapAbsenexcel($proyek_id){
		// mengambil data proses dari database dengan cara memanggil ke model
		$data['proses'] = $this->Home_model->getProses($proyek_id);
		$this->load->view('rekapAbsenexcel',$data);

	}
	public function detail_proyek($proyek_id){
		// menyimpan session user ke dalam variable $user
		$user = $this->session->userdata('username');
		// untuk mengecek apakah sudah login apa belum
		if ($user == "") {
			// redirect ke login
			header("location:".base_url()."home/login");
		}
		// mengambil data proyek dari database dengan cara memanggil ke model
		$data['proyek'] = $this->Home_model->getPerproyek($proyek_id);
		// mengambil data detail proyek berupa material untuk menampilkan semua detail material pada proyek tersebut yang ditentukan oleh parameter
		$data['material'] = $this->Home_model->getdetilproyek_material($proyek_id);
		// mengambil data detail proyek berupa pegawai untuk menampilkan semua detail pegawai pada proyek tersebut yang ditentukan oleh parameter
		$data['pegawai'] = $this->Home_model->getdetilproyek_pegawai($proyek_id);
		// mengambil semua data pegawai untuk menambahkan pegawai baru untuk proyek yang akan dikerjakan
		$data['pegawaiall'] = $this->Home_model->getpegawai();
		// mengambil semua data material untuk menambahkan material baru untuk proyek yang akan dikerjakan
		$data['materialall'] = $this->Home_model->getmaterial();
		// untuk menampilkan html ke user
		$this->load->view('header',$data);
		$this->load->view('detailproyek',$data);
		$this->load->view('footer',$data);
	}

	public function tambah_pegawai(){
		// deklarasi array untuk memasukkan kedalam database
		$peg_id = $this->input->post('peg_id');

		$data['peg_nama'] = $this->input->post('peg_nama');
		$data['peg_jabatan'] = $this->input->post('peg_jabatan');
		$data['peg_hp'] = $this->input->post('peg_hp');
		$data['peg_alamat'] = $this->input->post('peg_alamat');
		$data['peg_tingkat'] = 1;
		$data['peg_status'] = 1;
		// kondisi dimana untuk mengecek apakah insert atau update
		if ($peg_id == "") {
			// melakukan insert ke tabel pegawai
			$this->db->insert('pegawai', $data);
			$ubah = "Penambahan";
		}else {
			// melakukan update kedalam tabel pegawai
			$this->db->where('peg_id', $peg_id);
			$this->db->update('pegawai', $data);
			$ubah = "Perubahan";
		}
		// redirect langsung nag tampilan pegawai
		echo "<script>alert ('$ubah Pegawai Sukses ..');window.location.href = '".base_url()."home/pegawai';</script>";


	} 
	public function tambah_detilProyekpegawai(){
		// deklarasi array untuk memasukkan kedalam database
		$data['dp_id'] = $this->input->post('id_detilpegawai');
		$data['id_proyek'] = $this->input->post('id_proyek');
		$data['id_pegawai'] = $this->input->post('id_pegawai');
		$data['detail_pegawai'] = $this->input->post('detail_pegawai');
		$data['lama_pegawai'] = $this->input->post('lama_pegawai');
		$data['gaji_pegawai'] = $this->input->post('gaji_pegawai');
		$data['dp_status'] = 1;
		// kondisi dimana untuk mengecek apakah insert atau update
		if ($data['dp_id'] == "") {
			// melakukan insert ke tabel dp_pegawai atau detil pegawai pada proyek
			$this->db->insert('dp_pegawai', $data);
			$ubah = "Penambahan";
		}else {
			// melakukan update kedalam tabel dp_pegawai atau detil pegawai pada proyek
			$this->db->where('dp_id', $data['dp_id']);
			$this->db->update('dp_pegawai', $data);
			$ubah = "Perubahan";
		}
		// redirect langsung nag tampilan pegawai
		echo "<script>alert ('$ubah Detil Pegawai Sukses ..');window.location.href = '".base_url()."home/detail_proyek/".$data['id_proyek']."';</script>";
	}
	public function tambah_detilProyekmaterial(){
		$data['id_proyek'] = $this->input->post('id_proyek');
		$data['id_material'] = $this->input->post('id_material');
		$data['banyak'] = $this->input->post('banyak');
		$data['ket'] = $this->input->post('ket');
		$data['dp_status'] = 1;
		// kondisi dimana untuk mengecek apakah insert atau update
		if ($this->input->post('id_detilmaterial') == "") {
			// memanggil material dengan id untuk mendapatkan jumlah stok yang tersisa dan sehingga bisa ditambahkan dengan yang baru
			$material = $this->Home_model->getPermaterial($data['id_material']);
			$banyakMaterial = $material['master_stok'];
			// proses untuk penambahan stok yang lama dengan stok yang baru saja ditambahkan
			$materialupdate['master_stok'] = $banyakMaterial+$data['banyak'];
			// proses update kedatabase material untuk mengupdate data stok material yang ada
			$this->db->where('master_id', $data['id_material']);
			$this->db->update('master_material', $materialupdate);
			// deklarasi array yang akan disimpan dalam tabel history
			$history['master_id'] = $data['id_material'];
			$history['his_jumlah'] = $data['banyak'];
			$history['his_harga'] = $this->input->post('harga');
			$history['his_tanggal'] = date("Y-m-d");
			$history['his_status'] = 1;
			// insert ke dalam tabel history
			$this->db->insert('history_pembelian', $history);

			// melakukan insert ke tabel dp_material atau detil material pada proyek
			$this->db->insert('dp_material', $data);
			$ubah = "Penambahan";
		}else {
			// deklarasi array untuk memasukkan kedalam database
			$data['dp_id'] = $this->input->post('id_detilmaterial');
			// untuk mengambil dan menyimpan data banyak stok ditabel dp_material karena akan diupdate harus disimpan dahulu
			$material_detil = $this->Home_model->getPermaterial_detil($data['dp_id']);
			// variabel yang menyimpan banyak stok ditabel dp_material old
			$old_banyak = $material_detil['banyak'];
			// memanggil material dengan id untuk mendapatkan jumlah stok yang tersisa dan sehingga bisa ditambahkan dengan yang baru
			$material = $this->Home_model->getPermaterial($data['id_material']);
			$banyakMaterial = $material['master_stok'];

			$materialupdate['master_stok'] = ($banyakMaterial-$old_banyak)+$data['banyak'];
			// proses update kedatabase material untuk mengupdate data stok material yang ada
			$this->db->where('master_id', $data['id_material']);
			$this->db->update('master_material', $materialupdate);
			// deklarasi array yang akan disimpan dalam tabel history
			$history['master_id'] = $data['id_material'];
			$history['his_jumlah'] = $data['banyak'];
			$history['his_harga'] = $this->input->post('harga');
			$history['his_tanggal'] = date("Y-m-d");
			$history['his_status'] = 2;
			// insert ke dalam tabel history
			$this->db->insert('history_pembelian', $history);

			// melakukan update kedalam tabel  dp_material atau detil material pada proyek
			$this->db->where('dp_id', $data['dp_id']);
			$this->db->update('dp_material', $data);
			$ubah = "Perubahan";
		}
		// redirect langsung nag tampilan pegawai
		echo "<script>alert ('$ubah Detil Material Sukses ..');window.location.href = '".base_url()."home/detail_proyek/".$data['id_proyek']."';</script>";
	}
	public function tambah_material(){

		$data['master_nama'] = $this->input->post('master_nama');
		$data['master_stok'] = $this->input->post('master_stok');
		$data['master_tipe'] = $this->input->post('master_tipe');
		$data['master_satuan'] = $this->input->post('master_satuan');
		$data['master_status'] = 1;
		// kondisi dimana untuk mengecek apakah insert atau update
		if ($this->input->post('master_id') == "") {
			// melakukan insert ke tabel pegawai
			$this->db->insert('master_material', $data);
			$ubah = "Penambahan";
		}else {
			// deklarasi array untuk memasukkan kedalam database
			$data['master_id'] = $this->input->post('master_id');
			// melakukan update kedalam tabel pegawai
			$this->db->where('master_id', $data['master_id']);
			$this->db->update('master_material', $data);
			$ubah = "Perubahan";
		}
		// redirect langsung nag tampilan pegawai
		echo "<script>alert ('$ubah Material Sukses ..');window.location.href = '".base_url()."home/material';</script>";
	}
	public function tambah_proyek(){
		// deklarasi array untuk memasukkan kedalam database
		$proyek_id = $this->input->post('proyek_id');
		$data['proyek_nama'] = $this->input->post('proyek_nama');
		$data['proyek_awal'] = $this->input->post('proyek_awal');
		$data['proyek_akhir'] = $this->input->post('proyek_akhir');
		$data['proyek_alamat'] = $this->input->post('proyek_alamat');
		$data['mp_panjang'] = $this->input->post('mp_panjang');
		$data['mp_tinggi'] = $this->input->post('mp_tinggi');
		$data['mp_lebar'] = $this->input->post('mp_lebar');
		$valume = $data['mp_panjang'] * $data['mp_tinggi'] * $data['mp_lebar'];
		// echo "<br/>";
		$volume_asli = 16*6*3;
		$bagian = $valume/$volume_asli;
		$data['proyek_status'] = 1;
		// kondisi dimana untuk mengecek apakah insert atau update
		if ($proyek_id == "") {
			// melakukan insert ke tabel pegawai
			$this->db->insert('master_proyek', $data);
			$last_id = $this->db->insert_id();
			$dp['id_proyek'] = $last_id;
			$dp['dp_status'] = '1';
			$dp['id_material'] = '1';
			$dp['banyak'] = 15+(round((15/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '2';
			$dp['banyak'] = 20+(round((20/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '3';
			$dp['banyak'] = 15+(round((15/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '4';
			$dp['banyak'] = 200+(round((200/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '5';
			$dp['banyak'] = 10000+(round((10000/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '6';
			$dp['banyak'] = 30+(round((30/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '7';
			$dp['banyak'] = 40+(round((40/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '8';
			$dp['banyak'] = 25+(round((25/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '9';
			$dp['banyak'] = 25+(round((25/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '10';
			$dp['banyak'] = 10+(round((10/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '11';
			$dp['banyak'] = 20+(round((20/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '12';
			$dp['banyak'] = 25+(round((25/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '13';
			$dp['banyak'] = 200+(round((200/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '14';
			$dp['banyak'] = 6+(round((6/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '15';
			$dp['banyak'] = 10+(round((10/$bagian),2));
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);
			$dp['id_material'] = '16';
			$dp['banyak'] = '1';
			$this->db->insert('dp_material', $dp);
			$simpan = "update master_material set master_stok = (master_stok + ".$dp['banyak'].") 
						where master_id = '".$dp['id_material']."' ;";
			$query = $this->db->query($simpan);


			$ubah = "Penambahan";
		}else {
			// melakukan update kedalam tabel pegawai
			$this->db->where('proyek_id', $proyek_id);
			$this->db->update('master_proyek', $data);
			$ubah = "Perubahan";
		}
		// redirect langsung nag tampilan pegawai
		echo "<script>alert ('$ubah Proyek Sukses ..');window.location.href = '".base_url()."home/proyek';</script>";
	}
	public function tambah_progres(){
		// deklarasi array untuk memasukkan kedalam database
		$data['proyek_id'] = $this->input->post('proyek_ids');
		$data['progres_hari'] = $this->input->post('proyek_awal');
		$data['progres_pegawai'] = $this->input->post('progres_pegawai');
		$data['progres_absen'] = $this->input->post('absen');
		$data['progres_status'] = 1;
		// melakukan insert ke tabel pegawai
		$this->db->insert('progres', $data);
		$ubah = "Penambahan";
		// redirect langsung nag tampilan pegawai
		echo "<script>alert ('$ubah Proyek Sukses ..');window.location.href = '".base_url()."home/proses/".$data['proyek_id']."';</script>";
	}
	public function tambah_progres_mingguan(){
		// deklarasi array untuk memasukkan kedalam database
		$data['pm_proyek'] = $this->input->post('proyek_id');
		$pm_id = $this->input->post('pm_id');
		$dpm_ket = $this->input->post('dpm_ket');
		$dpm_persen = $this->input->post('dpm_persen');
		$data['pm_status'] = 1;
		if ($pm_id == '' || $pm_id == null ) {
			$data['pm_minggu'] = $this->input->post('pm_minggu');
			// melakukan insert ke tabel pegawai
			$this->db->insert('progress_mingguan', $data);
			$last_id = $this->db->insert_id();
			for ($i=0; $i < count($dpm_ket); $i++) { 
				$dpm['dpm_ket'] = $dpm_ket[$i];
				$dpm['dpm_progress'] = $last_id;
				$dpm['dpm_persen'] = $dpm_persen[$i];
				$dpm['dpm_status'] = 1;
				$this->db->insert('detail_pm', $dpm);
			}
			$ubah = "Penambahan";			
		}else {
			// melakukan update ke tabel pegawai
			$this->db->where('pm_id', $pm_id);
			$this->db->update('progress_mingguan', $data); 
			$ubah = "Pengubahan";
		}
		// redirect langsung nag tampilan pegawai
		echo "<script>alert ('$ubah Progress Sukses ..');window.location.href = '".base_url()."home/progress/".$data['pm_proyek']."';</script>";
	}
	public function tambah_finishing(){
		$master_id = $this->input->post('master_id');
		$value = $this->input->post('value');
		$id_proyek = $this->input->post('id_proyek');

		for ($i=0; $i < count($master_id); $i++) { 
			$history['master_id'] = $master_id[$i];
			$history['his_jumlah'] = $value[$i];
			$history['his_tanggal'] = date("Y-m-d");
			$history['his_status'] = 1;
			$this->db->insert('history_pembelian', $history);
			$dp_material = $this->Home_model->getdp_material($history['master_id'],$id_proyek);
			// print_r ($dp_materialsd);
			$stok_barang = $dp_material['banyak'];
			$sisa = $history['his_jumlah'];
			$material_id = $dp_material['id_material'];
			$simpan = "update master_material set master_stok = ((master_stok - $stok_barang )+$sisa) where master_id = '$material_id' ;";
			$query = $this->db->query($simpan);
			echo "<br/>"; 
		}
		$simpan = "update master_proyek set proyek_status = '0' where proyek_id = '$id_proyek' ;";
		$query = $this->db->query($simpan);

		echo "<script>alert ('Finishing Sukses ..');window.location.href = '".base_url()."home/proyek/';</script>";
	}
	public function progress($id_proyek){
		$data['progress'] = $this->Home_model->getprogressmingguan($id_proyek);
		$data['jml'] = $this->Home_model->jumlahminggu($id_proyek);
		$data['proyek_id'] = ($id_proyek);
		$this->load->view('header',$data);
		$this->load->view('progress',$data);
		$this->load->view('footer',$data);
	}
	public function detail_progress($id_proyek){
		$data['progress'] = $this->Home_model->getprogressmingguan($id_proyek);
		$data['jml'] = $this->Home_model->jumlahminggu($id_proyek);
		$data['proyek_id'] = ($id_proyek);
		$this->load->view('progress2',$data);
	}
	public function finishing($id_proyek){
		
		$simpan = "SELECT b.master_id, b.master_nama, banyak, dp_id
					FROM dp_material a
					JOIN master_material b ON b.master_id = a.id_material
					WHERE id_proyek = $id_proyek 
					and dp_status = 1";
		$query = $this->db->query($simpan);
		$data['finishing'] = $query->result_array();
		$data['id_proyek'] = $id_proyek;
		$this->load->view('finishing',$data);
	}
	public function detail_absen($id_proyek, $minggu){
		// echo $minggu." id proyek ".$id_proyek;
		if ($minggu == 1) {
			$awal = 0;
			$akhir = 7;
		}else if ($minggu == 2) {
			$awal = 8;
			$akhir = 14;
		}else if ($minggu == 3) {
			$awal = 15;
			$akhir = 21;
		}else if ($minggu == 4) {
			$awal = 22;
			$akhir = 28; 
		}else if ($minggu == 5) {
			$awal = 29;
			$akhir = 35;
		}
		$simpan = "SELECT c.peg_nama, DATE_FORMAT(progres_hari, '%d-%m-%Y') AS progres_hari, a.progres_absen
					FROM progres a
					JOIN master_proyek b ON b.proyek_id = a.proyek_id
					JOIN pegawai c ON c.peg_id = a.progres_pegawai
					WHERE a.proyek_id = $id_proyek
					AND progres_hari >= (DATE_ADD(proyek_awal, INTERVAL $awal DAY)) 
					AND progres_hari <= (DATE_ADD(proyek_awal, INTERVAL ($akhir) DAY)) ";
		$query = $this->db->query($simpan);
		$data['absen'] = $query->result_array();
		// print_r ($dat);
		$this->load->view('absen2',$data);
	}

	public function delete($peg_id){
		$data['peg_status'] = 2;
		$this->db->where('peg_id', $peg_id);
		$this->db->update('pegawai', $data);
		echo "<script>alert ('Hapus Pegawai Sukses ..');window.location.href = '".base_url()."home/pegawai';</script>";

	}
	public function login(){
		// menampilan tampilan login ke user
		$this->load->view('login',$data);
	}
	public function ceklogin(){
		// post ke controller untuk menyimpan username dan password
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		// fungsi untuk mengecek login apakah dalam tabel user ada atau tidak
		$data = $this->Home_model->login($username,$password);
		// kondisi dimana untuk mengecek apakah ada atau tidak datanya
		if($data == ""){
			// memberitahu user bahwa tidak aada data yang sudah dimasukkan
			echo "<script>alert ('Username and Password is Wrong !');window.location.href = '".base_url()."';</script>";
		}else if($data['level'] != ""){
			// menyimpan data yang sudah diselect dari database kedalam session ada 3 username, id dan level
			$this->session->set_userdata('username',$data['username']);
			$this->session->set_userdata('id',$data['id']);
			$this->session->set_userdata('level',$data['level']);
			echo "<script>window.location.href = '".base_url()."';</script>";
		}else {
			// tidak ada yang dilakukan dan redirect ke login
			echo "<script>window.location.href = '".base_url()."';</script>";
		}

	}
	public function logout(){
		// fungsi untuk mengahpus session yang sudah diset sebelumnya
		$this->session->unset_userdata(array('username' => ''));
		$this->session->unset_userdata(array('id' => ''));
		$this->session->unset_userdata(array('level' => ''));
		header("location:".base_url());
	}

}
