<!-- main content start-->
<div id="page-wrapper">
  <div class="main-page">
    <div class="tables">
      <h2 class="title1">Tables</h2>
      <div class="panel-body widget-shadow">
        <br>
        <hr>
        <h4>Proses Proyek (<?php echo $proyek['proyek_nama']; ?>)</h4>
        <form class="" action="<?php echo base_url(); ?>home/rekapAbsen" method="post">
                    <select class="form-control" name="proyek" id="proyek">
                        <?php foreach ($proyek2 as $value) { ?>
                        <option
                            value="<?php echo $value['proyek_id']; ?>"
                            <?php if($value['proyek_id'] == $proyek_id){ echo "selected";}?>><?php echo $value['proyek_nama']; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <hr>
                    <button type="submit" class="btn btn-primary" name="button">Cari</button>
                </form>
                <br> 
                <br> 
        <table class="table" id="example">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>Nama Pegawai</th>
              <th>Absen</th>
            </tr> 
          </thead>
          <tbody>
            <?php
            $i=1;
            $cektgl = 0;
            foreach ($proses as $value) {

              // if ($value['progres_hari'] != $cektgl) {
              //   $ulu = 'rowspan="2"';
              // }else {
              //   $ulu = '';
              //   $value['progres_hari'] = '';
              // }
              // $cektgl = $value['progres_hari'];
              ?>
              <tr>
                <th scope="row"><?php echo $i; ?></th>
                <td ><?php echo $value['progres_hari']; ?></td>
                <td><?php echo $value['peg_nama']; ?></td>
                <td><?php
                if ($value['progres_absen'] == 1) { 
                  echo "Masuk";
                }else {
                  echo "Tidak Masuk";
                }
                 ?></td>

              </tr>
            <?php
            $i++;
            }
            ?>
          </tbody>
        </table>
        <a href="javascript:cek();" class="btn btn-success" name="button">Export</a>

      </div>
    </div>
  </div>
</div>


<link href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css' media='all' rel='stylesheet' type='text/css'/>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>


<script>
	function cek(){
    let id = $("#proyek").val();
    window.location.assign('<?php echo base_url()?>home/rekapAbsenexcel/'+id);
  }
  $(document).ready(function() {
      $('#example').DataTable();
  } );
</script>