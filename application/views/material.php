

<!-- main content start-->
<div id="page-wrapper">
  <div class="main-page">
    <div class="tables">
      <h2 class="title1">Tables</h2>
      <div class="panel-body widget-shadow">
        <a href="javascript:tambah();">
          <button class="btn btn-success" type="button" name="button" data-toggle="modal" data-target="#myModal" >Tambah</button>
        </a>
        <br>
        <hr>
        <h4>Data Material:</h4>
        <table class="table" id="example">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Satuan</th>
              <th>Stok</th>
              <th>Tipe</th>
              <?php
          		$level = $this->session->userdata('level');
              if ($level != 1 && $level != 2) { ?>
              <th></th>
              <?php
              }
              ?>
              <!-- <th></th> -->
            </tr>
          </thead>
          <tbody>
            <?php
            $i=1;
            foreach ($material as $value) { ?>
              <tr>
                <th scope="row"><?php echo $i; ?></th>
                <td><?php echo $value['master_nama']; ?></td>
                <td><?php echo $value['master_satuan']; ?></td>
                <td><?php echo $value['master_stok']; ?></td>
                <td><?php
                if ($value['master_tipe'] == 1) {
                  echo "Material Kasar";
                }else if ($value['master_tipe'] == 1) {
                  echo "Material Finishing";
                }else if ($value['master_tipe'] == 1) {
                  echo "Transportasi";
                }else {
                  echo "Lainnya";
                }
                 ?></td>
                 <?php
                  $level = $this->session->userdata('level');
                  if ($level != 1 && $level != 2) { ?>
                  
                <td>
                  <a href="javascript:edit('<?php echo $value['master_id']; ?>','<?php echo $value['master_nama']; ?>','<?php echo $value['master_stok']; ?>','<?php echo $value['master_tie']; ?>','<?php echo $value['master_satuan']; ?>');">
                    <button class="btn btn-warning" type="button" name="button"  data-toggle="modal" data-target="#myModal"  >Edit</button>
                  </a>
                </td>
                  
                  <?php
                  }
                  ?>
                <!-- <td>
                  <a class="btn btn-danger" href="<?php echo base_url(); ?>home/delete/<?php echo $value['peg_id']; ?>" type="button" name="button">Hapus</a>
                </td> -->

              </tr>
            <?php
            $i++;
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- modal gan  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
    <form class="form-horizontal" action="<?php echo base_url(); ?>home/tambah_material" method="post">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
	  <div class="modal-body">
      <div class=" form-grids row form-grids-right">
					<div class="widget-shadow " data-example-id="basic-forms">
						<div class="form-title">
							<h4>Form Material :</h4>
						</div>
            <input type="hidden" name="master_id" id="master_id">
						<div class="form-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Material</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="master_nama" id="master_nama" placeholder="Masukkan Nama">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Satuan</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="master_satuan" id="master_satuan" placeholder="Masukkan Satuan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Stok</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="master_stok" id="master_stok" placeholder="Masukkan Stok">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tipe</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="master_tipe">
                      <option value="1">Material Kasar</option>
                      <option value="2">Material Finishing</option>
                      <option value="3">Transportasi</option>
                      <option value="4">Lainnya</option>
                    </select>
                  </div>
                </div>
						</div>
					</div>
				</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save</button>
	  </div>
  </form>
	</div>
  </div>
</div>

<!-- end modal -->


<link href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css' media='all' rel='stylesheet' type='text/css'/>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
	
  $(document).ready(function() {
      $('#example').DataTable();
  } );

  function tambah(){
    $("#master_id").val("");
    $("#master_nama").val("");
    $("#master_stok").val("");
    $("#master_tipe").val("");
    $("#master_satuan").val("");
  }

  function edit(id,nama,stok,tipe,satuan){
    $("#master_id").val(id);
    $("#master_nama").val(nama);
    $("#master_stok").val(stok);
    $("#master_tipe").val(tipe);
    $("#master_satuan").val(satuan);
  }
</script>
