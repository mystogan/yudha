<!-- main content start-->
<div id="page-wrapper">
  <div class="main-page">
    <div class="tables">
      <h2 class="title1">Tables</h2>
      <div class="panel-body widget-shadow">
        <h4>Basic Table:</h4>
        <table class="table">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Pegawai</th>
              <th>Jabatan</th>
              <th>HP</th>
              <th>Alamat</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $i=1;
            foreach ($pegawai as $value) { ?>
              <tr>
                <th scope="row"><?php echo $i; ?></th>
                <td><?php echo $value['peg_nama']; ?></td>
                <td><?php echo $value['peg_jabatan']; ?></td>
                <td><?php echo $value['peg_hp']; ?></td>
                <td><?php echo $value['peg_alamat']; ?></td>
              </tr>
            <?php
            $i++;
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
