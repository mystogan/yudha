<?php
class Home_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	function login($username,$password){
		$this->db->select('*');
		$this->db->from("users");
		$this->db->where('username = ', $username);
		$this->db->where('password = ', $password);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function GantiPassword($password){
		$this->db->select('*');
		$this->db->from("users");
		$this->db->where('password = ', $password);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getpegawai(){
		$this->db->select('*');
		$this->db->from("pegawai");
		$this->db->where('peg_status = 1');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getmaterial(){
		$this->db->select('*');
		$this->db->from("master_material");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getPermaterial($id_material){
		$this->db->select('*');
		$this->db->from("master_material");
		$this->db->where('master_id = ', $id_material);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getproyek($tahun = 0){
		$this->db->select('*');
		$this->db->select("DATE_FORMAT(proyek_awal, '%d-%m-%Y') AS proyek_awal",false);
		$this->db->select("DATE_FORMAT(proyek_akhir, '%d-%m-%Y') AS proyek_akhir",false);
		$this->db->select("DATE_FORMAT(proyek_awal, '%Y-%m-%d') AS proyek_awal1",false);
		$this->db->select("DATE_FORMAT(proyek_akhir, '%Y-%m-%d') AS proyek_akhir1",false);
		$this->db->from("master_proyek");
		if($tahun != 0){
			$this->db->where("DATE_FORMAT(proyek_awal, '%Y') = $tahun");
		}
		$this->db->where("proyek_status <= '1'");
		$this->db->order_by("proyek_id", "desc"); 
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getPerproyek($proyek_id = ''){
		$this->db->select('*');
		$this->db->from("master_proyek");
		$this->db->select("DATE_FORMAT(proyek_awal, '%Y-%m-%d') AS proyek_awal1",false);
		$this->db->select("DATE_FORMAT(proyek_akhir, '%Y-%m-%d') AS proyek_akhir1",false);
		$this->db->where('proyek_id = ', $proyek_id);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getdetilproyek_material($proyek_id = ''){
		$this->db->select('a.*,b.master_nama,b.master_satuan');
		$this->db->from("dp_material a");
		$this->db->join('master_material b', 'b.master_id = a.id_material');
		$this->db->where('id_proyek = ', $proyek_id);
		$this->db->where('a.dp_status = ', '1');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getPermaterial_detil($id_detil = ''){
		$this->db->select('*');
		$this->db->from("dp_material");
		$this->db->where('dp_id = ', $id_detil);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getdp_material($id_material = '',$id_proyek){
		$this->db->select('*');
		$this->db->from("dp_material");
		$this->db->where('dp_id = ', $id_material);
		$this->db->where('id_proyek = ', $id_proyek);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}

	function getdetilproyek_pegawai($proyek_id = ''){
		$this->db->select('a.*,b.peg_nama,b.peg_id');
		$this->db->from("dp_pegawai a");
		$this->db->join('pegawai b', 'b.peg_id = a.id_pegawai');
		$this->db->where('id_proyek = ', $proyek_id);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getProses($proyek_id = ''){
		$this->db->select('a.*,b.proyek_nama,c.peg_nama');
		$this->db->from("progres a");
		$this->db->join('master_proyek b', 'b.proyek_id = a.proyek_id');
		$this->db->join('pegawai c', 'c.peg_id = a.progres_pegawai');
		$this->db->where('a.proyek_id = ', $proyek_id);
		$this->db->order_by("a.progres_hari", "asc");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getprogressmingguan($proyek_id = ''){
		$this->db->select('a.*,b.proyek_nama');
		$this->db->from("progress_mingguan a");
		$this->db->join('master_proyek b', 'b.proyek_id = a.pm_proyek');
		$this->db->where('a.pm_proyek = ', $proyek_id);
		$this->db->order_by("a.pm_minggu", "asc");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function jmlProgres($proyek_id = ''){
		$this->db->select('*');
		$this->db->from("progress_mingguan");
		$this->db->where('pm_proyek = ', $proyek_id);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function hasil($proyek_id = ''){
		$strSql = "SELECT COUNT(DISTINCT(progres_hari)) AS hasil_kemajuan,
								(DATEDIFF(b.proyek_akhir, b.proyek_awal)-COUNT(DISTINCT(progres_hari))) AS hasil_yang_belum,
								DATEDIFF(b.proyek_akhir, b.proyek_awal) AS total_proyek
								FROM progres a
								JOIN master_proyek b ON a.proyek_id = b.proyek_id
								WHERE a.proyek_id = $proyek_id;";
			$query = $this->db->query($strSql);
			$data = $query->result_array();
			return $data[0];
	}
	function jumlahminggu($proyek_id = ''){
		$strSql = "SELECT DATEDIFF(proyek_akhir, proyek_awal) AS selisih,
						(DATEDIFF(proyek_akhir, proyek_awal)/7) AS bagi,
						CEIL ((DATEDIFF(proyek_akhir, proyek_awal)/7)) AS bulat
					FROM master_proyek
					WHERE proyek_id = $proyek_id;";
			$query = $this->db->query($strSql);
			$data = $query->result_array();
			return $data[0];
	}
	
	function getRekap($proyek_id = ''){
		$this->db->select('a.progres_absen,b.peg_id,b.peg_nama,b.peg_jabatan, COUNT(a.progres_status) AS hasil',false);
		$this->db->from("progres a");
		$this->db->join('pegawai b ', 'a.progres_pegawai = b.peg_id');
		$this->db->where('a.proyek_id  = ', $proyek_id);
		$this->db->where('a.progres_status = 1');
		$this->db->where('a.progres_absen = 1');
		$this->db->group_by("a.progres_pegawai");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getPegawaiGaji($proyek_id = ''){
		$this->db->select('peg_id,peg_nama,dp_id,gaji_pegawai',false);
		$this->db->from("pegawai");
		$this->db->join('dp_pegawai', 'peg_id = id_pegawai');
		$this->db->where('id_proyek  = ', $proyek_id);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}

}
?>
