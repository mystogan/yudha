/*
SQLyog Community v12.2.6 (64 bit)
MySQL - 10.1.25-MariaDB : Database - simkonstruksi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `detail_pm` */

CREATE TABLE `detail_pm` (
  `dpm_id` int(11) NOT NULL AUTO_INCREMENT,
  `dpm_progress` int(11) DEFAULT NULL,
  `dpm_ket` varchar(1000) DEFAULT NULL,
  `dpm_persen` int(11) DEFAULT NULL,
  `dpm_status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`dpm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `detail_pm` */

insert  into `detail_pm`(`dpm_id`,`dpm_progress`,`dpm_ket`,`dpm_persen`,`dpm_status`) values 
(1,5,'tembok',75,1),
(2,5,'depan ',75,1),
(3,5,'belakang',75,1),
(4,6,'kamar orang tua ',80,1),
(5,6,'kamar tengah',100,1);

/*Table structure for table `dp_material` */

CREATE TABLE `dp_material` (
  `dp_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_proyek` int(11) DEFAULT NULL,
  `id_material` int(11) DEFAULT NULL,
  `banyak` int(11) DEFAULT NULL,
  `ket` varchar(1000) DEFAULT NULL,
  `dp_status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`dp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `dp_material` */

insert  into `dp_material`(`dp_id`,`id_proyek`,`id_material`,`banyak`,`ket`,`dp_status`) values 
(1,1,1,17,NULL,1),
(2,1,2,23,NULL,1),
(3,1,3,17,NULL,1),
(4,1,4,225,NULL,1),
(5,1,5,11250,NULL,1),
(6,1,6,34,NULL,1),
(7,1,7,45,NULL,1),
(8,1,8,28,NULL,1),
(9,1,9,28,NULL,1),
(10,1,10,11,NULL,1),
(11,1,11,23,NULL,1),
(12,1,12,28,NULL,1),
(13,1,13,225,NULL,1),
(14,1,14,7,NULL,1),
(15,1,15,11,NULL,1),
(16,1,16,1,NULL,1);

/*Table structure for table `dp_pegawai` */

CREATE TABLE `dp_pegawai` (
  `dp_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_proyek` int(11) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `detail_pegawai` varchar(200) DEFAULT NULL,
  `gaji_pegawai` int(11) DEFAULT NULL,
  `lama_pegawai` int(11) DEFAULT NULL,
  `dp_status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`dp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `dp_pegawai` */

insert  into `dp_pegawai`(`dp_id`,`id_proyek`,`id_pegawai`,`detail_pegawai`,`gaji_pegawai`,`lama_pegawai`,`dp_status`) values 
(1,2,2,'detail nama pengawas',100000,10,1),
(4,2,1,'asd',200000,3,1),
(5,4,3,'hehe',10000,10,1),
(6,4,5,'ipo',80000,10,1),
(7,4,2,'sapi',50000,10,1);

/*Table structure for table `history_pembelian` */

CREATE TABLE `history_pembelian` (
  `his_id` int(20) NOT NULL AUTO_INCREMENT,
  `master_id` int(20) DEFAULT NULL,
  `his_jumlah` int(20) DEFAULT NULL,
  `his_harga` int(20) DEFAULT NULL,
  `his_tanggal` date DEFAULT NULL,
  `his_status` int(10) DEFAULT NULL COMMENT '1 insert, 2 update',
  PRIMARY KEY (`his_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `history_pembelian` */

insert  into `history_pembelian`(`his_id`,`master_id`,`his_jumlah`,`his_harga`,`his_tanggal`,`his_status`) values 
(1,1,10,100000,'2018-12-09',1),
(2,1,8,100000,'2018-12-09',2),
(3,2,20,50000,'2018-12-09',1),
(4,2,25,250000,'2018-12-22',1),
(5,2,25,250000,'2018-12-22',1);

/*Table structure for table `master_material` */

CREATE TABLE `master_material` (
  `master_id` int(20) NOT NULL AUTO_INCREMENT,
  `master_nama` varchar(50) DEFAULT NULL,
  `master_satuan` varchar(20) DEFAULT NULL,
  `master_stok` int(20) DEFAULT NULL,
  `master_status` int(10) DEFAULT NULL,
  `master_tipe` int(20) DEFAULT NULL,
  PRIMARY KEY (`master_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `master_material` */

insert  into `master_material`(`master_id`,`master_nama`,`master_satuan`,`master_stok`,`master_status`,`master_tipe`) values 
(1,'Batu Pondasi','Kubik',138,1,1),
(2,'Pasir','Kubik',127,1,1),
(3,'Koral','Kubik',93,1,1),
(4,'Semen','sak',1237,1,1),
(5,'bata merah','biji',61874,1,1),
(6,'kayu kaso','batang',186,1,1),
(7,'bambu','batang',245,1,1),
(8,'triplex','lembar',152,1,1),
(9,'plapon','lembar',152,1,1),
(10,'besi 8','batang',59,1,1),
(11,'kanal c','batang',127,1,1),
(12,'galvalum','lembar',152,1,1),
(13,'keramik','dus/paket',1237,1,1),
(14,'cat','pill',39,1,1),
(15,'kran air','biji',59,1,1),
(16,'klosed','biji',13,1,1);

/*Table structure for table `master_proyek` */

CREATE TABLE `master_proyek` (
  `proyek_id` int(20) NOT NULL AUTO_INCREMENT,
  `proyek_nama` varchar(50) DEFAULT NULL,
  `proyek_awal` datetime DEFAULT NULL,
  `proyek_akhir` datetime DEFAULT NULL,
  `proyek_alamat` text,
  `proyek_luas` int(11) DEFAULT NULL,
  `mp_panjang` int(11) NOT NULL,
  `mp_tinggi` int(11) NOT NULL,
  `mp_lebar` int(11) NOT NULL,
  `proyek_status` int(10) DEFAULT NULL,
  PRIMARY KEY (`proyek_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `master_proyek` */

insert  into `master_proyek`(`proyek_id`,`proyek_nama`,`proyek_awal`,`proyek_akhir`,`proyek_alamat`,`proyek_luas`,`mp_panjang`,`mp_tinggi`,`mp_lebar`,`proyek_status`) values 
(1,'masjid alhamdulillah','2019-01-14 17:46:26','2019-01-31 00:00:00','surabaya',NULL,16,3,6,1);

/*Table structure for table `pegawai` */

CREATE TABLE `pegawai` (
  `peg_id` int(20) NOT NULL AUTO_INCREMENT,
  `peg_nama` varchar(50) DEFAULT NULL,
  `peg_jabatan` varchar(50) DEFAULT NULL,
  `peg_hp` varchar(30) DEFAULT NULL,
  `peg_alamat` text,
  `peg_tingkat` varchar(50) DEFAULT NULL,
  `peg_status` int(20) DEFAULT NULL,
  PRIMARY KEY (`peg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `pegawai` */

insert  into `pegawai`(`peg_id`,`peg_nama`,`peg_jabatan`,`peg_hp`,`peg_alamat`,`peg_tingkat`,`peg_status`) values 
(1,'Nama Pegawai WE mas ','jabatan WE','hp WE','alamat WE','1',1),
(2,'nama pegawau','jabatan pegawai','no hp pegawai','alamat pegawai ','1',1),
(3,'Yudha ','Programmer','0823456789','surabaya','1',1),
(4,'namo','wkwkwkww alamat','wkwkw hp','wkwkwk alamat','1',1),
(5,'Mikail','Tukang','0851425341637','Surabaya','1',1);

/*Table structure for table `progres` */

CREATE TABLE `progres` (
  `progres_id` int(20) NOT NULL AUTO_INCREMENT,
  `proyek_id` int(20) DEFAULT NULL,
  `progres_hari` date DEFAULT NULL,
  `progres_pegawai` int(11) DEFAULT NULL,
  `progres_absen` int(11) DEFAULT NULL,
  `progres_status` int(20) DEFAULT NULL,
  PRIMARY KEY (`progres_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `progres` */

insert  into `progres`(`progres_id`,`proyek_id`,`progres_hari`,`progres_pegawai`,`progres_absen`,`progres_status`) values 
(1,2,'2018-12-18',2,1,1),
(2,2,'2018-12-10',1,1,1),
(3,2,'2018-12-10',2,1,1),
(4,2,'2018-12-13',1,1,1),
(5,2,'2019-01-09',2,1,1),
(6,2,'2018-12-30',1,1,2),
(7,2,'2018-12-30',2,1,1),
(9,4,'2019-01-07',5,1,1),
(10,4,'2019-01-07',2,1,1),
(11,4,'2019-01-08',2,1,1),
(12,4,'2019-01-08',3,1,1),
(13,4,'2019-01-07',3,1,1),
(15,4,'2019-01-09',3,2,1);

/*Table structure for table `progress_mingguan` */

CREATE TABLE `progress_mingguan` (
  `pm_id` int(11) NOT NULL AUTO_INCREMENT,
  `pm_proyek` int(11) DEFAULT NULL,
  `pm_minggu` int(11) DEFAULT NULL,
  `pm_ket` text,
  `pm_status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`pm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `progress_mingguan` */

insert  into `progress_mingguan`(`pm_id`,`pm_proyek`,`pm_minggu`,`pm_ket`,`pm_status`) values 
(1,2,1,'pengubahan jos lek iya iya lek',1),
(2,2,2,'ini adalah detail laporan keduan tidak ada kendala dalam pemasangan',1),
(3,4,1,'minggu pertama opo ae persen tase cak ',1),
(4,4,2,'mbuh laporan seng kedua ',1),
(5,1,1,NULL,1),
(6,1,2,NULL,1);

/*Table structure for table `users` */

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`level`) values 
(1,'direktur','28b662d883b6d76fd96e4ddc5e9ba780',1),
(2,'ketua','28b662d883b6d76fd96e4ddc5e9ba780',2),
(3,'admin','28b662d883b6d76fd96e4ddc5e9ba780',3),
(4,'pelaksana','28b662d883b6d76fd96e4ddc5e9ba780',4);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
