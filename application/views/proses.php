<!-- main content start-->
<div id="page-wrapper">
  <div class="main-page">
    <div class="tables">
      <h2 class="title1">Tables</h2>
      <div class="panel-body widget-shadow">
        <a href="javascript:tambah();">
          <button class="btn btn-success" type="button" name="button" data-toggle="modal" data-target="#myModal" >Tambah</button>
        </a>
        <br>
        <hr>
        <h4>Proses Proyek (<?php echo $proyek['proyek_nama']; ?>)</h4>
        <table class="table">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Proyek</th>
              <th>Tanggal</th>
              <th>Nama Pegawai</th>
              <th>Absen</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $i=1;
            $cektgl = 0;
            foreach ($proses as $value) {

              if ($value['progres_hari'] != $cektgl) {
                $ulu = 'rowspan="2"';
              }else {
                $ulu = '';
                $value['progres_hari'] = '';
              }
              $cektgl = $value['progres_hari'];
              ?>
              <tr>
                <th scope="row"><?php echo $i; ?></th>
                <td><?php echo $value['proyek_nama']; ?></td>
                <td ><?php echo $value['progres_hari']; ?></td>
                <td><?php echo $value['peg_nama']; ?></td>
                <td><?php
                if ($value['progres_absen'] == 1) {
                  echo "Masuk";
                }else {
                  echo "Tidak Masuk";
                }
                 ?></td>

              </tr>
            <?php
            $i++;
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- modal gan  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
    <form class="form-horizontal" action="<?php echo base_url(); ?>home/tambah_progres" method="post">
      <input type="hidden" name="proyek_ids" value="<?php echo $proyek['proyek_id']; ?>">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
	  <div class="modal-body">
      <div class=" form-grids row form-grids-right">
					<div class="widget-shadow " data-example-id="basic-forms">
						<div class="form-title">
							<h4>Form Tambah :</h4>
						</div>
						<div class="form-body">

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Pegawai</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="progres_pegawai">
                      <?php
                      foreach ($pegawai as $value) { ?>
                        <option value="<?php echo $value['peg_id']; ?>"><?php echo $value['peg_nama']; ?></option>
                      <?php
                      }
                       ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
                  <div class="col-sm-9">
                    <input type="date" min="<?php echo $proyek['proyek_awal1']; ?>" max="<?php echo $proyek['proyek_akhir1']; ?>" class="form-control" name="proyek_awal" id="proyek_awal">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Absen</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="absen">
                        <option value="1">Ya</option>
                        <option value="2">Tidak</option>
                    </select>
                  </div>
                </div>

						</div>
					</div>
				</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save</button>
	  </div>
  </form>
	</div>
  </div>
</div>

<!-- end modal -->
