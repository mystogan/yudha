<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page">
        <div class="tables">
            <h2 class="title1">Tables</h2>
            <div class="panel-body widget-shadow">
                <br>
                <hr>
                <h4>Ganti Password:</h4>
                <form action="<?php echo base_url()?>home/cekGantiPassword" method="POST">

                    <div class="form-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Password Lama</label>
                            <div class="col-sm-9">
                                <input
                                    type="password"
                                    class="form-control"
                                    name="password_lama"
                                    placeholder="Masukkan Password Lama">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Password Baru</label>
                            <div class="col-sm-9">
                                <input
                                    type="password"
                                    class="form-control"
                                    name="password_baru"
                                    placeholder="Masukkan Password Baru">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Konfirmasi Password Baru</label>
                            <div class="col-sm-9">
                                <input
                                    type="password"
                                    class="form-control"
                                    name="konfirmasi_password_baru"
                                    placeholder="Masukkan Konfirmasi Password Baru">
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary" >Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>