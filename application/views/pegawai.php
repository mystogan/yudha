

<!-- main content start-->
<div id="page-wrapper">
  <div class="main-page">
    <div class="tables">
      <h2 class="title1">Tables</h2>
      <div class="panel-body widget-shadow">
        <a href="javascript:tambah();">
          <button class="btn btn-success" type="button" name="button" data-toggle="modal" data-target="#myModal" >Tambah</button>
        </a>
        <br>
        <hr>
        <h4>Data Pegawai:</h4>
        <table class="table" id="example">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Pegawai</th>
              <th>Jabatan</th>
              <th>HP</th>
              <th>Alamat</th>
              <?php
          		$level = $this->session->userdata('level');
              if ($level != 1 && $level != 2) { ?>
              <th></th>
              <th></th>
              
              <?php
              }
              ?>
            </tr>
          </thead>
          <tbody>
            <?php
            $i=1;
            foreach ($pegawai as $value) { ?>
              <tr>
                <th scope="row"><?php echo $i; ?></th>
                <td><?php echo $value['peg_nama']; ?></td>
                <td><?php echo $value['peg_jabatan']; ?></td>
                <td><?php echo $value['peg_hp']; ?></td>
                <td><?php echo $value['peg_alamat']; ?></td>
                <?php
                $level = $this->session->userdata('level');
                if ($level != 1 && $level != 2) { ?>
                <td>
                  <a href="javascript:edit('<?php echo $value['peg_id']; ?>','<?php echo $value['peg_nama']; ?>','<?php echo $value['peg_jabatan']; ?>','<?php echo $value['peg_hp']; ?>','<?php echo $value['peg_alamat']; ?>');">
                    <button class="btn btn-warning" type="button" name="button"  data-toggle="modal" data-target="#myModal"  >Edit</button>
                  </a>
                </td>
                <td>
                  <a class="btn btn-danger" href="<?php echo base_url(); ?>home/delete/<?php echo $value['peg_id']; ?>" type="button" name="button">Hapus</a>
                </td>
                
                <?php
                }
                ?>


              </tr>
            <?php
            $i++;
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- modal gan  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
    <form class="form-horizontal" action="<?php echo base_url(); ?>home/tambah_pegawai" method="post">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
	  <div class="modal-body">
      <div class=" form-grids row form-grids-right">
					<div class="widget-shadow " data-example-id="basic-forms">
						<div class="form-title">
							<h4>Form Tambah :</h4>
						</div>
            <input type="hidden" name="peg_id" id="peg_id">
						<div class="form-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Pegawai</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="peg_nama" id="peg_nama" placeholder="Masukkan Nama">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jabatan</label>
                  <div class="col-sm-9">
                  <select name="peg_jabatan" id="peg_jabatan" class="form-control">
                    <option value="Mandor">Mandor</option>
                    <option value="Tukang">Tukang</option>
                    <option value="Tuli">Tuli</option>
                  </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">No HP</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="peg_hp" id="peg_hp" placeholder="Masukkan No HP">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="peg_alamat" id="peg_alamat" placeholder="Masukkan Alamat">
                  </div>
                </div>
						</div>
					</div>
				</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save</button>
	  </div>
  </form>
	</div>
  </div>
</div>

<!-- end modal -->

<link href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css' media='all' rel='stylesheet' type='text/css'/>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
	
  $(document).ready(function() {
      $('#example').DataTable();
  } );

  function tambah(){
    $("#peg_id").val("");
    $("#peg_nama").val("");
    $("#peg_jabatan").val("");
    $("#peg_hp").val("");
    $("#peg_alamat").val("");

  }

  function edit(id,nama,jabatan,hp,alamat){
    $("#peg_id").val(id);
    $("#peg_nama").val(nama);
    $("#peg_jabatan").val(jabatan);
    $("#peg_hp").val(hp);
    $("#peg_alamat").val(alamat);

  }
</script>
