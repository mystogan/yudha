<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page">
        <div class="tables">
            <h2 class="title1">Tables</h2>
            <div class="panel-body widget-shadow">
                <a href="javascript:tambah();">
                    <button
                        class="btn btn-success"
                        type="button"
                        name="button"
                        data-toggle="modal"
                        data-target="#myModal">Tambah</button>
                </a>
                <br>
                <hr>
                <h4>Data Proyek:</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Proyek</th>
                            <th>Minggu Ke</th>
                            <th>Keterangan Proyek</th>
                            <th></th>
                            <!-- <th></th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
            $i=1;
            foreach ($progress as $value) { ?>
                        <tr>
                            <th scope="row"><?php echo $i; ?></th>
                            <td><?php echo $value['proyek_nama']; ?></td>
                            <td><?php echo $value['pm_minggu']; ?></td>
                            <td><?php 
                                $this->db->select('*');
                                $this->db->from("detail_pm");
                                $this->db->where('dpm_progress = ', $value['pm_id']);
                                $query = $this->db->get();
                                $datak = $query->result_array();
                                foreach ($datak as $key => $value) {
                                  print_r ($value['dpm_ket']." => ".$value['dpm_persen']."% <br/>");
                                }             
                            ?></td>
                            <td>
                                <a
                                    href="javascript:edit('<?php echo $value['pm_id']; ?>','<?php echo $value['pm_minggu']; ?>','<?php echo $value['pm_ket']; ?>');">
                                    <button
                                        class="btn btn-warning"
                                        type="button"
                                        name="button"
                                        data-toggle="modal"
                                        data-target="#myModal">Edit</button>
                                </a>
                            </td>
                            <!-- <td> <a class="btn btn-primary" href="<?php echo base_url();
                            ?>home/detail_proyek/<?php echo $value['proyek_id']; ?>" type="button"
                            name="button">Detail</a> </td> -->

                        </tr>
                        <?php
            $i++;
            }
            ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- modal gan -->
<div
    class="modal fade"
    id="myModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form
                class="form-horizontal"
                action="<?php echo base_url(); ?>home/tambah_progres_mingguan"
                method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class=" form-grids row form-grids-right">
                        <div class="widget-shadow " data-example-id="basic-forms">
                            <div class="form-title">
                                <h4>Form Tambah :</h4>
                            </div>
                            <input type="hidden" name="pm_id" id="pm_id">
                            <input
                                type="hidden"
                                name="proyek_id"
                                id="proyek_id"
                                value="<?php echo $proyek_id?>">
                            <div class="form-body">
                                <div class="form-group" id="minggu_div">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Minggu ke-</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="pm_minggu" name="pm_minggu">
                                            <?php 
                                              for ($i=0; $i < $jml['bulat'] ; $i++) {  
                                                $cek = 0;
                                                foreach ($progress as $value) {
                                                  if ($value['pm_minggu'] == ($i+1)) {
                                                    $cek = 1;
                                                  }
                                                }
                                                if ($cek != 1) {  
                                                ?>
                                            <option value="<?php echo ($i+1);?>"><?php echo ($i+1);?></option>
                                            <?php
                                                }
                                              }
                                              ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-sm-12" id="hehe">
                                    <div id="clone">
                                        <div class="col-sm-3">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Laporan Progress</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input
                                                type="text"
                                                name="dpm_ket[]"
                                                class="form-control"
                                                placeholder="Masukkan Keterangan">
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" name="dpm_persen[]" value="100" class="form-control" placeholder="50">
                                        </div>
                                        <div class="col-sm-1">
                                            %
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                                <label for="l"> <a href="javascript:clone();"> Tambah Keterangan</a></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- end modal -->

<script type="text/javascript">
    function tambah() {
        var x = document.getElementById("minggu_div");
        x.style.display = "block";
        $("#pm_id").val("");
        $("#pm_minggu").val("");
        $("#pm_ket").val("");

    }

    function edit(pm_id, pm_minggu, pm_ket) {
        var x = document.getElementById("minggu_div");
        x.style.display = "none";
        $("#pm_id").val(pm_id);
        $("#pm_minggu").val(pm_minggu);
        $("#pm_ket").val(pm_ket);
    }

    function clone(){
      var div = document.getElementById('clone'),
          clone = div.cloneNode(true); 
      clone.id = "clone2";
      document.getElementById("hehe").appendChild(clone);
    }
</script>