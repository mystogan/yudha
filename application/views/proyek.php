<!-- main content start-->
<div id="page-wrapper">
  <div class="main-page">
    <div class="tables">
      <h2 class="title1">Tables</h2>
      <div class="panel-body widget-shadow">
        <!-- <a href="javascript:tambah();">
          <button class="btn btn-success" type="button" name="button" data-toggle="modal" data-target="#myModal" >Tambah</button>
        </a> -->
        <br>
        <hr>
        <h4>Data Proyek:</h4>
        <form action="<?php echo base_url();?>home/proyek" method="POST">
        <select name="thisYear" id="thisYear" class="form-control">
          <?php
          // echo $thisYear;
          for ($i=0; $i < 5; $i++) { ?>
            <option value="<?php echo (date("Y")-$i)?>" <?php if($thisYear == (date("Y")-$i)){echo "selected"; } ?> ><?php echo (date("Y")-$i)?></option>
          <?php  
          }
          ?>
        </select>
        <br>
        <button type="submit" class="btn btn-primary"> Cari</button>
        </form>
        <hr>
        <table class="table" id="example">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Proyek</th>
              <th>Tanggal Mulai Proyek</th>
              <th>Tanggal Akhir Proyek</th>
              <th>Alamat Proyek</th>
              <th>Volume Proyek</th>
              <th></th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php
            $i=1;
            foreach ($proyek as $value) { ?>
              <tr>
                <th scope="row"><?php echo $i; ?></th>
                <td><?php echo $value['proyek_nama']; ?></td>
                <td><?php echo $value['proyek_awal']; ?></td>
                <td><?php echo $value['proyek_akhir']; ?></td>
                <td><?php echo $value['proyek_alamat']; ?></td>
                <td><?php echo $value['mp_panjang']*$value['mp_tinggi']*$value['mp_lebar']; ?>
                m3</td>
                <?php
                $level = $this->session->userdata('level');
                if($value['proyek_status'] != 0 && $level != 1 && $level != 2){ ?>
                <td>
                  <a class="btn btn-info" href="<?php echo base_url(); ?>home/proses/<?php echo $value['proyek_id']; ?>" type="button" name="button">Absen</a>
                </td>
                <td>
                    <a
                        class="btn btn-primary"
                        href="<?php echo base_url(); ?>home/detail_proyek/<?php echo $value['proyek_id']; ?>"
                        type="button"
                        name="button">Detail</a>
                </td>
                <td>
                  <a class="btn btn-primary" href="<?php echo base_url(); ?>home/progress/<?php echo $value['proyek_id']; ?>" type="button" name="button">Progress</a>
                </td>
                
                <?php
                }
                ?>

              </tr>
            <?php
            $i++;
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- modal gan  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
    <form class="form-horizontal" action="<?php echo base_url(); ?>home/tambah_proyek" method="post">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
	  <div class="modal-body">
      <div class=" form-grids row form-grids-right">
					<div class="widget-shadow " data-example-id="basic-forms">
						<div class="form-title">
							<h4>Form Tambah :</h4>
						</div>
            <input type="hidden" name="proyek_id" id="proyek_id">
						<div class="form-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama Proyek</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="proyek_nama" id="proyek_nama" placeholder="Masukkan Nama">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Mulai</label>
                  <div class="col-sm-9">
                    <input type="date" class="form-control" name="proyek_awal" id="proyek_awal" placeholder="Masukkan Jabatan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Akhir</label>
                  <div class="col-sm-9">
                    <input type="date" class="form-control" name="proyek_akhir" id="proyek_akhir" placeholder="Masukkan No HP">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alamat Proyek</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="proyek_alamat" id="proyek_alamat" placeholder="Masukkan Alamat">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Panjang Proyek</label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control" name="mp_panjang" id="mp_panjang" placeholder="Masukkan Panjang Proyek" value="16">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Lebar Proyek</label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control" name="mp_lebar" id="mp_lebar" placeholder="Masukkan Lebar" value="6.5">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tinggi Proyek</label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control" name="mp_tinggi" id="mp_tinggi" placeholder="Masukkan Tinggi" value="3">
                  </div>
                </div>
						</div>
					</div>
				</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save</button>
	  </div>
  </form>
	</div>
  </div>
</div>


<!-- end modal -->

<link href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css' media='all' rel='stylesheet' type='text/css'/>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>


<script type="text/javascript">	
  $(document).ready(function() {
      $('#example').DataTable();
  } );
  function tambah(){
    document.getElementById("proyek_awal").readOnly  = false;
    $("#proyek_id").val("");
    $("#proyek_nama").val("");
    $("#proyek_awal").val("");
    $("#proyek_akhir").val("");
    $("#proyek_alamat").val("");
    $("#proyek_luas").val("");

  }

  function edit(id,nama,jabatan,hp,alamat,luas){
    document.getElementById("proyek_awal").readOnly  = true;
    $("#proyek_id").val(id);
    $("#proyek_nama").val(nama);
    $("#proyek_awal").val(jabatan);
    $("#proyek_akhir").val(hp);
    $("#proyek_alamat").val(alamat);
    $("#proyek_luas").val(luas);
  }
</script>
