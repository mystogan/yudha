<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />


<h4>Data Proyek:</h4>
<table class="table" id="example">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Pegawai</th>
            <th>Hari</th>
            <th>Absen</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $i=1;
            foreach ($absen as $value) { ?>
        <tr>
            <th scope="row"><?php echo $i; ?></th>
            <td><?php echo $value['peg_nama']; ?></td>
            <td><?php echo $value['progres_hari']; ?></td>
            <td><?php 
            if ($value['progres_absen'] == 1) {
                echo "Ya";
            }else {
                echo "Tidak";
            }
             ?></td>

        </tr>
        <?php
            $i++;
            }
            ?>
    </tbody>
</table>

<link href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css' media='all' rel='stylesheet' type='text/css'/>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>


<script>
  $(document).ready(function() {
      $('#example').DataTable();
  } );
</script>