<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page">
        <div class="tables">
            <h2 class="title1">Tables</h2>
            <div class="panel-body widget-shadow">
                <h4>Laporan Progres Proyek (<?php echo $proyek['proyek_nama']; ?>):</h4>
                <form class="" action="<?php echo base_url(); ?>home/index" method="post">
                    <select class="form-control" name="proyek">
                        <?php foreach ($proyek2 as $value) { ?>
                        <option
                            value="<?php echo $value['proyek_id']; ?>"
                            <?php if($value['proyek_id'] == $proyek['proyek_id']){ echo "selected";}?>><?php echo $value['proyek_nama']; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <hr>
                    <button type="submit" class="btn btn-primary" name="button">Cari</button>
                </form>
                <hr>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

                    </div>
                    <div class="col-md-6">
                        <div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<div
    class="modal fade"
    id="myModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class=" form-grids row form-grids-right" id="load_progress"></div>
            </div>
        </div>
    </div>
</div>
<div
    class="modal fade"
    id="absen"
    tabindex="-1"
    role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class=" form-grids row form-grids-right" id="load_absen"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    // Build the chart
    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Grafik Progres <?php echo $proyek['proyek_nama']; ?>'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [
            {
                name: 'Hari',
                colorByPoint: true,
                point: {
                    events: { 
                        click: function (event) {
                            // alert(this.x + " " + this.y);
                            if(this.x == 0){
                                $("#load_progress").load("<?php echo base_url();?>home/detail_progress/<?php echo $id_proyek; ?>");
                                $("#myModal").modal();
                            }
                        }
                    }
                },
                data: [
                    {
                        name: 'Sudah Dilaksanakan',
                        y: <?php echo $progress_proyek; ?>
                    }, {
                        name: 'Belum Dilaksanakan',
                        y: <?php echo $hasil_yang_belum; ?>
                    }
                ]
            }
        ]
    });

    Highcharts.chart('container2', {
        chart: {
            type: 'line',
            events: {
	                click: function(event) {
                        var simpan = Math.round(event.xAxis[0].value)+parseInt(1);
	                    // alert (simpan);
                        $("#load_absen").load("<?php echo base_url();?>home/detail_absen/<?php echo $id_proyek; ?>/"+simpan);
                        $("#absen").modal();
	                }
	            }
        },
        title: {
            text: 'Jumlah Pegawai Absen '
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [<?php
      $i=1;
      foreach ($jumlah as $value){
        echo "'Minggu ke-".$i."',";
        $i++;
      }
      
      ?>]
        },
        yAxis: {
            title: {
                text: 'Jumlah Absen Pegawai'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [
            {
                name: 'Pegawai',
                data: [<?php
                        foreach ($jumlah as $value){
                            echo $value['hasil'].",";
                        }
                        ?>]
            }
        ]
    });
</script>