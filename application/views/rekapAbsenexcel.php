<?php 
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=laporan_absen.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");

?>

<table class="table" id="example">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>Nama Pegawai</th>
              <th>Absen</th>
            </tr> 
          </thead>
          <tbody>
            <?php 
            $i=1;
            $cektgl = 0;
            foreach ($proses as $value) {

              // if ($value['progres_hari'] != $cektgl) {
              //   $ulu = 'rowspan="2"';
              // }else {
              //   $ulu = '';
              //   $value['progres_hari'] = '';
              // }
              // $cektgl = $value['progres_hari'];
              ?>
              <tr>
                <th scope="row"><?php echo $i; ?></th>
                <td ><?php echo $value['progres_hari']; ?></td>
                <td><?php echo $value['peg_nama']; ?></td>
                <td><?php
                if ($value['progres_absen'] == 1) {
                  echo "Masuk";
                }else {
                  echo "Tidak Masuk";
                }
                 ?></td>

              </tr>
            <?php
            $i++;
            }
            ?>
          </tbody>
        </table>