<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page">
        <div class="tables">
            <h2 class="title1">Tables</h2>
            <div class="panel-body widget-shadow">
                <br>
                <hr>
                <h4>Rekap Pegawai:</h4>
                <form class="" action="<?php echo base_url(); ?>home/rekap" method="post">
                    <select class="form-control" name="proyek" id="proyek">
                        <?php foreach ($proyek2 as $value) { ?>
                        <option
                            value="<?php echo $value['proyek_id']; ?>"
                            <?php if($value['proyek_id'] == $proyek_id){ echo "selected";}?>><?php echo $value['proyek_nama']; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <hr>
                    <button type="submit" class="btn btn-primary" name="button">Cari</button>
                </form>
                <hr>
                <table class="table" id="example">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Jabatan</th>
                            <th>Gaji</th>
                            <th>Jumlah Absen</th>
                            <th>Jumlah Gaji</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i=1;
                        foreach ($rekap as $value) { ?>
                        <tr>
                            <th scope="row"><?php echo $i; ?></th>
                            <td><?php echo $value['peg_nama']; ?></td>
                            <td><?php echo $value['peg_jabatan']; ?></td>
                            <td>
                              <?php
								
                                foreach ($gaji as $Hgaji) {
                                  if ($value['peg_id'] == $Hgaji['peg_id']) {
                                    $gajigan = $Hgaji['gaji_pegawai'];
                                    echo "Rp " . number_format($gajigan,2,',','.');
                                    break;
                                  }  
                                }
                              ?>
                            </td>
                            <td><?php echo $value['hasil']; ?></td>
                            <td><?php 
							if($value['progres_absen'] == 1){
								$jumlah = $gajigan * $value['hasil'];
								echo "Rp " . number_format($jumlah,2,',','.');		
							}
                            
                            ?></td>

                        </tr>
                        <?php
                        $i++;
                        }
                        ?>
                    </tbody>
                </table>
                <a href="javascript:cek();" class="btn btn-success" name="button">Export</a>
            </div>
        </div>
    </div>
</div>


<link href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css' media='all' rel='stylesheet' type='text/css'/>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>


<script>
  function cek(){
    let id = $("#proyek").val();
    window.location.assign('<?php echo base_url()?>home/rekapgaji/'+id);
  }
  $(document).ready(function() {
      $('#example').DataTable();
  } );
</script>